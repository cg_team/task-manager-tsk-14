package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
